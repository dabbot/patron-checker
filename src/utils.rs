use serenity::model::id::{GuildId, RoleId, UserId};
use serenity::CACHE;
use std::fmt::Write;

pub fn role_diff<'a>(
    guild_id: GuildId,
    user_id: UserId,
    old_roles: &'a [RoleId],
    new_roles: &'a [RoleId],
) -> Option<(String, Vec<&'a RoleId>, Vec<&'a RoleId>)> {
    let role_ids = [
        RoleId(301828565085716480), // supporter
        RoleId(301781206347939841), // super supporter
        RoleId(301781366155247616), // super duper supporter
    ];

    let added_ids = new_roles
        .iter()
        .filter(|id| !old_roles.contains(id))
        .filter(|id| role_ids.contains(*id))
        .collect::<Vec<&RoleId>>();
    let removed_ids = old_roles
        .iter()
        .filter(|id| !new_roles.contains(id))
        .filter(|id| role_ids.contains(id))
        .collect::<Vec<&RoleId>>();

    if added_ids.is_empty() && removed_ids.is_empty() {
        return None;
    }

    let cache = CACHE.read();

    let mut content = {
        let found = cache.user(user_id).unwrap();
        let user = found.read();

        format!(
            "<@87164639695110144> <@114941315417899012>\n```diff\n{} ({})\n",
            user.tag(),
            user.id,
        )
    };

    {
        let guild = cache.guild(guild_id).unwrap();
        let reader = guild.read();

        for role_id in &added_ids {
            let role = &reader.roles[role_id];

            let _ = write!(content, "+ {} ({})\n", role.name, role.id);
        }

        for role_id in &removed_ids {
            let role = &reader.roles[role_id];

            let _ = write!(content, "- {} ({})\n", role.name, role.id);
        }
    }

    content.push_str("```");

    Some((content, added_ids, removed_ids))
}
