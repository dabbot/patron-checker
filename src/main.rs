#[macro_use] extern crate log;

extern crate env_logger;
extern crate kankyo;
extern crate postgres;
extern crate serenity;

mod handler;
mod utils;

use handler::Handler;
use postgres::{Connection, TlsMode};
use serenity::client::Client;
use serenity::model::id::{ChannelId, GuildId, RoleId};
use std::sync::{Arc, Mutex};
use std::env;

pub const PATRON_ROLES: [RoleId; 3] = [
    RoleId(301828565085716480),
    RoleId(301781206347939841),
    RoleId(301781366155247616),
];
pub const STAFF_ROLE: RoleId = RoleId(325307197666099200);

fn main() {
    kankyo::load().expect("Error loading .env");
    env_logger::init();

    let connection = Connection::connect(
        env::var("POSTGRES_ADDR").expect("No POSTGRES_ADDR").as_ref(),
        TlsMode::None,
    ).expect("Err creating connection");

    let token = env::var("DISCORD_TOKEN").expect("No DISCORD_TOKEN");
    let guild_id = env::var("GUILD_ID")
        .expect("no GUILD_ID")
        .parse()
        .map(GuildId)
        .expect("invalid GUILD_ID");
    let patron_log_id = env::var("PATRON_LOG_ID")
        .expect("no PATRON_LOG_ID")
        .parse()
        .map(ChannelId)
        .expect("invalid PATRON_LOG_ID");

    let handler = Handler {
        connection: Arc::new(Mutex::new(connection)),
        guild_id,
        patron_log_id,
    };

    let mut client = Client::new(&token, handler).expect("Err creating client");

    if let Err(why) = client.start_autosharded() {
        error!("Error running client: {:?}", why);
    }
}
