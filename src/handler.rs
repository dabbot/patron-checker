use postgres::Connection;
use serenity::client::{Context, EventHandler};
use serenity::model::channel::Message;
use serenity::model::gateway::{Activity, Ready};
use serenity::model::guild::Member;
use serenity::model::id::{ChannelId, GuildId, RoleId, UserId};
use serenity::model::user::{OnlineStatus, User};
use std::sync::{Arc, Mutex};
use super::{PATRON_ROLES, STAFF_ROLE};
use utils;

pub struct Handler {
    pub connection: Arc<Mutex<Connection>>,
    pub guild_id: GuildId,
    pub patron_log_id: ChannelId,
}

impl Handler {
    fn add(&self, user_id: UserId, added: &[&RoleId]) {
        if added.is_empty() {
            return;
        }

        let conn = self.connection.lock().expect("Error unlocking connection");

        for role in added {
            let query = include_str!("../sql/add_role.sql");

            let res = conn.execute(query, &[
                &(user_id.0 as i64),
                &(role.0 as i64),
            ]);

            if let Err(why) = res {
                warn!(
                    "Error adding role {} to user {}: {:?}",
                    role,
                    user_id,
                    why,
                );
            }
        }
    }

    fn remove(&self, user_id: UserId, removed: &[&RoleId]) {
        if removed.is_empty() {
            return;
        }

        let conn = self.connection.lock().expect("Error unlocking connection");

        for role in removed {
            let query = include_str!("../sql/remove_role.sql");

            let res = conn.execute(query, &[
                &(user_id.0 as i64),
                &(role.0 as i64),
            ]);

            if let Err(why) = res {
                warn!(
                    "Error removing role {} from user {}: {:?}",
                    role,
                    user_id,
                    why,
                );
            }
        }
    }

    fn remove_user(&self, user_id: UserId) {
        let conn = self.connection.lock().expect("Error unlocking connection");

        let res = conn.execute(include_str!("../sql/remove_user.sql"), &[
            &(user_id.0 as i64),
        ]);

        if let Err(why) = res {
            warn!("Error removing patron {}: {:?}", user_id, why);
        }
    }

    fn send_diff(&self, text: &str) {
        if let Err(why) = self.patron_log_id.say(text) {
            warn!("Err sending patron log diff: {:?}; diff: {}", why, text);
        }
    }
}

impl EventHandler for Handler {
    fn guild_member_addition(
        &self,
        _: Context,
        _: GuildId,
        member: Member,
    ) {
        let user_id = member.user.read().id;
        let empty = Vec::new();

        let (text, added, removed) = match utils::role_diff(member.guild_id, user_id, &empty, &member.roles) {
            Some(diff) => diff,
            None => return,
        };

        self.add(user_id, added.as_slice());
        self.remove(user_id, removed.as_slice());
        self.send_diff(&text);
    }

    fn guild_member_removal(
        &self,
        _: Context,
        _: GuildId,
        user: User,
        _: Option<Member>,
    ) {
        self.remove_user(user.id);
    }

    fn guild_member_update(
        &self,
        _: Context,
        old: Option<Member>,
        new: Member,
    ) {
        if new.guild_id != self.guild_id {
            return;
        }

        let user_id = new.user.read().id;
        let old_role_ids = old.map(|old| old.roles).unwrap_or_default();

        let (text, added, removed) = match utils::role_diff(new.guild_id, user_id, &old_role_ids, &new.roles) {
            Some(diff) => diff,
            None => return,
        };

        self.add(user_id, added.as_slice());
        self.remove(user_id, removed.as_slice());
        self.send_diff(&text);
    }

    fn message(&self, _: Context, msg: Message) {
        if msg.content == "<@409463948438405150> ping" {
            let _ = msg.reply("Pong!");

            return;
        }

        if msg.content != "<@409463948438405150> patron fill" {
            return;
        }

        let guild = msg.guild().expect("Error getting guild");

        let reader = guild.read();

        let member = match reader.members.get(&msg.author.id) {
            Some(member) => member,
            None => return,
        };

        if !member.roles.contains(&STAFF_ROLE) {
            return;
        }

        let users = reader.members.iter().filter_map(|(_, ref m)| {
            let mut has = vec![];

            for role in PATRON_ROLES.iter() {
                if m.roles.contains(role) {
                    has.push(role);
                }
            }

            if has.is_empty() {
                None
            } else {
                Some((m.user.read().id, has))
            }
        }).collect::<Vec<_>>();

        for (user_id, roles) in users {
            let conn = self.connection.lock()
                .expect("Error unlocking connection");

            let res = conn.execute(include_str!("../sql/add_patron.sql"), &[
                &(user_id.0 as i64),
                &roles.iter().map(|x| x.0 as i64).collect::<Vec<i64>>(),
            ]);

            if let Err(why) = res {
                warn!("Err adding user {} to DB: {:?}", user_id, why);
            }
        }
    }

    fn ready(&self, ctx: Context, ready: Ready) {
        info!("Ready as {}!", ready.user.name);

        ctx.set_presence(Some(Activity::playing("dab")), OnlineStatus::DoNotDisturb);
    }
}
