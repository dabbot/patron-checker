insert into patrons (
    user_id,
    role_ids
) values (
    $1,
    '{}'
) on conflict (user_id) do update set
    role_ids = array_remove(role_ids, $2);
