insert into patrons (
    user_id,
    role_ids
) values (
    $1,
    $2::bigint[]
) on conflict (user_id) do update set
    role_ids = $2::bigint[]
