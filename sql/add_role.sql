insert into patrons (
    user_id,
    role_ids
) values (
    $1,
    array[$2]
) on conflict (user_id) do update set
    role_ids = array_append(role_ids, $2);
